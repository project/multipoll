README.txt for Multi-Question Polls 1.0.x

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Features
 * Limitations
 * Installation
 * Troubleshooting
 * Support/Customizations
 * Maintainers


INTRODUCTION
------------

Multi-Question Polls is a replacement for Drupal's poll module and provides 
multiple questions per poll and management options for authenticated users. You 
can create unlimited polls per site. Answers are stored in the database. Scores 
and results are displayed during or after the voting.

* To submit bug reports and feature suggestions, or to track changes:
   https://www.drupal.org/project/issues/multipoll


FEATURES
--------

* Time limited voting
* Single or Multiple choice answers


LIMITATIONS
-----------

Currently only authenticated users can take part in the voting. You can help to 
adopt voting system for guests.


INSTALLATION
------------

 * Install the Multi-Question Polls module as you would normally install a
   contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


TROUBLESHOOTING
---------------

If you have any problems feel free to create an issue 
https://www.drupal.org/node/add/project-issue/multipoll


SUPPORT/CUSTOMIZATIONS
----------------------

Support by volunteers is available on:

 * https://www.drupal.org/project/issues/multipoll?version=All&status=All

Please consider helping others as a way to give something back to the community
that provides Drupal and the contributed modules to you free of charge.

For paid support and customizations of this module, help with implementing an
Multi-Question Polls module, or other Drupal work, contact the maintainer 
through his contact form:

 * https://www.drupal.org/u/dillix


MAINTAINERS
-----------

 * Mikhail Khasaya (dillix) - https://www.drupal.org/u/dillix

Supporting organizations:

 * Dillix Media -
   https://dillix.com
